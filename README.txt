
Markup2
=======

A cloneable version of the popular Markup module, drupal.org/project/markup.

Q: When should you use Markup2 instead of Markup?

A: When you clone entities and entityforms (via this patch 
https://www.drupal.org/node/1816402#comment-11662679), you probably expect
a new instance be created. But with Markup this does NOT happen. So when you 
change the markup in the clone, you change the markup in the original.
With Markup, there is just the one shared instance.
With Markup2, there is one instance per clone.

